# bz_individual_4
This project showcases the development and deployment of two AWS Lambda functions using Rust. These functions are integral components of a data processing pipeline managed by AWS Step Functions. The initial function, data_occurrence, computes the frequency of each number within a provided string. Subsequently, the sort_by_occurrence function arranges these numbers based on their occurrence, prioritizing higher frequencies, and then further sorts them by value in the event of equivalent occurrences.

## Video demo
find the video tutorial [here](https://youtu.be/OOpeHQRi6Go)


## Procedures
1. Clone the repository
2. Install the AWS CLI and Rust
3. Configure the AWS CLI
4. Build the Rust project
5. Deploy the Lambda functions
6. Create the Step Functions state machine
7. Execute the Step Functions state machine

## Setup step function 
1. Create a new state machine
2. Add the following states:
    - data_occurrence
    - sort_by_occurrence
3. Connect the states with the appropriate input/output paths
4. Execute the state machine

## Build
```bash
cargo build --release
```

## Deploy
```bash
aws lambda create-function --function-name data_occurrence --zip-file fileb://./target/lambda/release/data_occurrence.zip --handler data_occurrence --runtime provided --role arn:aws:iam::123456789012:role/lambda-role --environment Variables={RUST_BACKTRACE=1}
```

## step function
![Alt Text](./img/stepfn.png)

```bash
{
  "Comment": "A simple state machine for data processing",
  "StartAt": "input",
  "States": {
    "FetchData": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-2:558669332806:function:fetch_data",
      "Next": "order"
    },
    "order": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-2:558669332806:function:transform_data",
      "End": true
}
```

## screenshots
![Alt Text](./img/build.png)

![Alt Text](./img/deploy.png)

![Alt Text](./img/input.png)

![Alt Text](./img/order.png)

