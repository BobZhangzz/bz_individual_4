use lambda_runtime::{service_fn, LambdaEvent, Error, Context};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Deserialize, Serialize)]
struct Input {
    string_data: String,
}

#[derive(Deserialize, Serialize)]
struct Output {
    frequency: HashMap<String, usize>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Input>) -> Result<Output, Error> {
    let (event, _context): (Input, Context) = event.into_parts();
    let mut frequency = HashMap::new();
    
    for num in event.string_data.split(',') {
        *frequency.entry(num.to_string()).or_insert(0) += 1;
    }
    
    Ok(Output { frequency })
}